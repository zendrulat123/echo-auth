# echo auth

1. Create a form 
e.GET("/", accessible)
//form
 <form style="padding:2%;margin:2%;" action="/login" method="POST">
            <label for="username">Username</label>
            <input type="text" id="username" name="username" value="jj"><br>
            <label for="password">Password</label>
            <input type="text" id="password" name="password" value="aa">
            <input type="submit" value="Submit">
        </form>


2. Post to e.POST("/login", login)

//login handler
func login(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	// Throws unauthorized error
	if username != "jj" || password != "aa" {
		return echo.ErrUnauthorized
	}
    passhash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
        if err != nil {
            fmt.Println(err)
            if err != nil {
                fmt.Println(err, "Err: Password Encryption  failed")
            }
            json.NewEncoder(c.Response().Writer).Encode(err)
        }
    usernamehash, err := bcrypt.GenerateFromPassword([]byte(username), bcrypt.DefaultCost)
        if err != nil {
            fmt.Println(err)
            if err != nil {
                fmt.Println(err, "Err: Password Encryption  failed")
            }
            json.NewEncoder(c.Response().Writer).Encode(err)
        }

    
	// Set custom claims
	claims := &jwtCustomClaims{
		passhash,
		true,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}
	// Create token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return err
	}
	cookie := new(http.Cookie)
	cookie.Name = usernamehash
	cookie.Value = t
	cookie.Expires = time.Now().Add(24 * time.Hour)
	c.SetCookie(cookie)
	return c.Render(http.StatusOK, "reached.html", map[string]interface{}{})

}

3. be able to grab data from the claims/token 

// Restricted group
	r := e.Group("/r")
	r.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		Claims:      &jwtCustomClaims{},
		SigningKey:  []byte("secret"),
		TokenLookup: "cookie:username",
	}))
	r.GET("", restricted)
	r.GET("/tester", Tester)
	
//where user ends up
func Tester(c echo.Context) error {

	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*jwtCustomClaims)
	name := claims.Name

	return c.Render(http.StatusOK, "tester.html", map[string]interface{}{})

}

